/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbb
 *
 */
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
 super(t); 
 } 
 public String toString() 
 { 
 float value = this.getValue();
 return value + " F"; 
 }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	float value = ((this.getValue() - 32) * 5)/9;
	Celsius cel = new Celsius(value);
	return cel;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	Fahrenheit fah = new Fahrenheit(this.getValue());
	return fah;
}
@Override
public Temperature toKelvin() {
	// TODO Auto-generated method stub
	float value = this.getValue();
	Fahrenheit fah = new Fahrenheit(value);
	Temperature cel = fah.toCelsius();
	value = cel.getValue() + 273;
	return new Kelvin(value);
} 
} 
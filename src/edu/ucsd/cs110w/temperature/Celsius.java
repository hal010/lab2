/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbb
 *
 */
public class Celsius extends Temperature 
{  
 public Celsius(float t)
 {
 super(t); 
 } 
 public String toString() 
 { 
 float value = this.getValue();
 return value + " C"; 
 }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	Celsius cel = new Celsius(this.getValue());
	return cel;
}
@Override
public Temperature toFahrenheit() {
	float value = ((9 * this.getValue())/5) + 32;
	Fahrenheit fah = new Fahrenheit(value);
	return fah;
}
@Override
public Temperature toKelvin() {
	// TODO Auto-generated method stub
	float value = this.getValue() + 273;
	return new Kelvin(value);
} 
} 
